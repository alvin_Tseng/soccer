//
//  Date+Addition.swift
//  soccer
//
//  Created by Alvin on 2021/5/7.
//

import Foundation

extension Date{
    /// 獲取時間戳記
    /// - Returns: 時間戳記
    static func getNowTimeTimestamp()->String{
        let now:Date = NSDate.now;
        let nowString:String =  String(format: "%.0f", now.timeIntervalSince1970)
        //只取到整數
        return nowString
    }
}
