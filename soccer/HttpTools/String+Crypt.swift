//
//  String+Crypt.swift
//  soccer
//
//  Created by Alvin on 2021/5/7.
//

import Foundation

extension String{
    
    /// 生成隨機字串
    /// - Parameter length: 隨機字串長度
    static func randomStringWithLength(length:Int){
        let letters:String = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        var randomString = String()
        for _ in 0...length{
            randomString += String(letters.randomElement()!)
        }
        print(randomString)
    }
}
